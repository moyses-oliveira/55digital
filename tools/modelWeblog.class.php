<?php
class modelWeblog {
	public static function posts($alias, $start, $limit, $language='pt_br', $search=null){
		$api = new API();
		$params = array(
			'int_start'=>$start,
			'int_limit'=>$limit, 
			'vrc_name'=>$search, 
			'vrc_alias'=>$alias, 
			'chr_language'=>$language
		);
		$api->addAction('cms','cms_list_weblog_post','run', $params);
		return $api->callMethod()->data;
	}
	
	public static function post($alias, $id = null, $language='pt_br'){
		$api = new API();
		if(!!$id):
			$params = array(
				'postd_int_post'=>$id,
				'wbl_vrc_alias'=>$alias, 
				'postd_chr_language'=>$language
			);
			$api->addAction('cms','cms_get_weblog_post_description_by_alias','run', $params);
		else:
			$params = array(
				'int_start' => 0,
				'int_limit' => 1, 
				'vrc_name' => null, 
				'vrc_alias' => $alias, 
				'chr_language' => $language
			);
			$api->addAction('cms','cms_list_weblog_post','run', $params);
		endif;
		$res = $api->callMethod();
		$data = $res->data;

		if(count($data)):
			return $data[0];
		endif;
		return (object)array(
				'post_int_id'=>null,
				'post_int_wblcat'=>null, 
				'post_dtt_posted'=>null, 
				'category'=>null,
				'postd_vrc_title'=>null, 
				'postd_vrc_summary'=>null, 
				'postd_txt_description'=>null, 
				'postd_chr_language'=>null
			);
	}
	
}