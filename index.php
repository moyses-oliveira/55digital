<?php
date_default_timezone_set("America/Sao_Paulo"); ## TIMEZONE
header('Content-Type: text/html; charset=UTF-8');


define('FRAMEWORK_PATH', '../framework/');
$GLOBALS['AUTOLOAD'] = array();
$GLOBALS['AUTOLOAD'][] = (object) array('path'=> FRAMEWORK_PATH . 'tools/', 'sufix'=>'.class.php');
$GLOBALS['AUTOLOAD'][] = (object) array('path'=> FRAMEWORK_PATH . 'tools/', 'sufix'=>'.php');
$GLOBALS['AUTOLOAD'][] = (object) array('path'=> 'tools/', 'sufix'=>'.class.php');
$GLOBALS['AUTOLOAD'][] = (object) array('path'=> FRAMEWORK_PATH . 'tools/YaLinqo/', 'sufix'=>'.php');
require_once FRAMEWORK_PATH . 'autoload.php';

$_SERVER['REQUEST_SCHEME'] = 'http';
session_name(md5($_SERVER['SERVER_NAME']));
session_start();

define('ROOT_BASE', trim(URL::root()));
require_once('controllers/controller.class.php');
require_once('controllers/bootstrap.class.php');
bootstrap::init();