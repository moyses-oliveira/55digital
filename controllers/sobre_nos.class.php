<?php 
class sobre_nos extends controller { 
	public static function _config()
	{
		static::$data->title = 'Sobre nós';
		static::$summary = 'Somos uma agência de desenvolvimento websites e soluções para internet com o propósito de cuidar do patrimônio digital dos nossos clientes para alcançar visibilidade de mercado.';
		static::$data->headBg = H::root() . 'files/img/layout/corp.jpg';
	}
	
	public static function index()
	{
		H::css(array('home.css'));
		static::_render('about-us.php');
	}
}