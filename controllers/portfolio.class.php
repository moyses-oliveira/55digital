<?php 
class portfolio extends controller { 
	public static function _config()
	{
		static::$data->title = 'Home';
		static::$data->headBg = H::root() . 'files/img/layout/portfolio.png';
	}
	
	public static function index()
	{
		static::_render('portfolio.php');
	}
}