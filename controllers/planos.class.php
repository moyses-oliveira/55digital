<?php 
class planos extends controller { 
	public static function _config()
	{
		static::$data->title = 'Home';
		static::$summary = 'Nossa estrutura nos permite atender a todas as necessidades padrão de um cliente que deseja ter visibilidade na internet.';
		static::$data->headBg = H::root() . 'files/img/layout/canvas.jpg';
	}
	
	public static function index()
	{
		static::_render('planos.php');
	}
}