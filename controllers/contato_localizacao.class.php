<?php

class contato_localizacao extends controller {

    public static function _config() {
        static::$data->title = 'Fale conosco';
        static::$summary = 'Deixe sua mensagem e responderemos rapidamente.';
        static::$data->headBg = H::root() . 'files/img/layout/contact.jpg';
    }

    public static function index() {
        H::css(array('home.css'));
        static::_render('contato_localizacao.php');
    }

    public static function submit() {
        if (isset($_POST['name'])):
            extract($_POST);
            $content = sprintf('
                    <h4><strong>Nome</strong>: %s </h4>
                    <h4><strong>E-mail</strong>: %s </h4>
                    <p>%s</p>
                    ',
                    $name,
                    $email,
                    str_replace('\n', '</p><p>', $message)
            );

            //$success = FormMail::send('contato@55digital.com.br', $subject, $content, 'contato@55digital.com.br', '55 DIGITAL FORM', true);
            $success = static::smtpmailer('contato@55digital.com.br', 'contato@55digital.com.br', '55 DIGITAL FORM', $subject, $content);
            echo json_encode(array('success' => $success));
            return;
        endif;
        echo json_encode(array('success' => false));
        return;
    }

    public static function smtpmailer($from, $to, $toName, $subject, $content) {
        try {
            $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug = 0;  // Debugar: 1 = erros e mensagens, 2 = mensagens apenas
            $mail->SMTPAuth = true; 
            $mail->SMTPSecure = getenv('SMTP_SECURITY');
            $mail->Host = getenv('SMTP_HOST');
            $mail->Port = getenv('SMTP_PORT'); 
            $mail->Username = getenv('SMTP_EMAIL');
            $mail->Password = getenv('SMTP_PASS');
           
            $mail->SetFrom($to, $toName);
            $mail->Subject = $subject;
            $mail->Body = $content;
            $mail->IsHTML(true);
            $mail->AddAddress($from);
            return !!$mail->Send();
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

}
