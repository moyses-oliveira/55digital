<?php 
class blog extends controller { 
	public static function _config()
	{
		static::$data->title = 'Blog';
		static::$data->headBg = H::root() . 'files/img/layout/services.png';
	}
	
	public static function index()
	{
		$page = URL::getVar('page');
		$limit = 10;
		$current_page = $page ? $page : 1;
		if(!$current_page) $current_page = 1;
		$start = $limit * ($current_page - 1);
		
		$blog = modelWeblog::posts('blog', $start, $limit);
		$totalrecords = count($blog) ? current($blog)->total_records : 0;
		static::$data->pag = new Pagination($current_page, $limit, $totalrecords);
		static::$data->blog = $blog;
		H::css(array('home.css'));
		static::_render('blog.php');
	}
}