<?php 
class depoimentos extends controller { 
	public static function _config()
	{
		static::$data->title = 'Home';
		static::$data->headBg = H::root() . 'files/img/layout/testimonials.jpg';
	}
	
	public static function index()
	{
		H::css(array('home.css'));
		static::_render('depoimentos.php');
	}
}