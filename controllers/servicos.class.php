<?php 
class servicos extends controller { 
	public static function _config()
	{
		static::$data->title = 'Serviços';
		static::$summary = 'Nossa estrutura nos permite atender a todas as necessidades padrão de um cliente que deseja ter visibilidade na internet.';
		static::$data->headBg = H::root() . 'files/img/layout/services.png';
	}
	
	public static function index()
	{
		H::css(array('home.css'));
		static::_render('servicos.php');
	}
}