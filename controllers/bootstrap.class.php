<?php
class bootstrap extends controller {
    public static function init()
    {
		static::$data = new stdClass();
		
		H::css(array(
			'//fonts.googleapis.com/css?family=Montserrat:700,400',
			'//fonts.googleapis.com/css?family=Playfair+Display:400,700,900,400italic,700italic,900italic&amp;subset=latin,latin-ext,cyrillic',
			'//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900',
			'bootstrap.css',
			'bootstrap-theme.css', 
			'font-awesome.css', 
			'wolverine/styles.css', 
			'slick/slick.css', 
			'jquery.fullPage.css',
			'main.css'
		));
		H::js(array(
			'modernizr/modernizr-2.8.3-respond-1.4.2.min.js',
			'jquery-1.11.2.min.js',
			'jquery.snsShare.js',
			'bootstrap.min.js',
			'stellar/jquery.stellar.min.js',
			'waypoints/lib/jquery.waypoints.min.js',
			'smooth-scroll/SmoothScroll.js',
			'slick/slick.js',
			'magnific-popup/jquery.magnific-popup.js',
			'isotope/isotope.pkgd.min.js',
			'isotope/packery-mode.pkgd.min.js',
			'img-loaded/imagesloaded.pkgd.min.js',
			'countto/jquery.countTo.js',
			'bootstrap-progressbar/bootstrap-progressbar.min.js',
			'progressbar/dist/progressbar.min.js',
			'main.js'
		));
		
		H::addMeta(array('charset'=>'utf-8'));
		H::addMeta(array('name'=>'viewport', 'content'=>'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'));
		H::addMeta(array('http-equiv'=>'X-UA-Compatible', 'content'=>'IE=edge'));
		H::addMeta(array('property'=>'og:locale', 'content'=>'pt_BR'));
		H::addMeta(array('property'=>'og:url', 'content'=>$_SERVER['REQUEST_SCHEME'] . ':' . URL::atual()));
		H::addMeta(array('property'=>'og:type', 'content'=>'website'));
		H::addMeta(array('property'=>'og:site_name', 'content'=> '55 Digital'));
		
		H::root(URL::root());
		$apiConf = json_decode(file_get_contents('files/configs/api.json'));
		foreach($apiConf as $k=>$v)
			define($k, $v);

		static::$api = new API();
		$settings = static::getSettings();
        
		list($module, $action, static::$cod) = array(URL::friend(0), URL::friend(1), URL::friend(2));
        if (!$module)
            $module = 'home';
		
		if(substr($module,-5) == '.html')
			$module = substr($module,0,-5);

        if (!$action)
            $action = 'index';
		
		$class = str_replace('-','_',$module);
		$ctrlFile = sprintf('controllers/%s.class.php',$class);

		if(!file_exists($ctrlFile))
			static::_redirect('er404');
			
		require_once $ctrlFile;

        if (method_exists($class, '_config'))
            call_user_func($class . '::_config');
			
        if (method_exists($class, 'setAction'))
            $action = call_user_func($class . '::setAction');

        if (!method_exists($class, $action))
            static::_redirect('er404');

        return call_user_func_array($class . '::' . $action, array());
    }
}