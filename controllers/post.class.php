<?php 
class post extends controller { 

	public static function _config()
	{
		static::$data->headBg = H::root() . 'files/img/layout/services.png';
	}
	
	public static function setAction(){ return 'index'; }
	
	public static function index()
	{
		$post_int_id = URL::getCode(URL::uri());
		static::$data->post = modelWeblog::post('blog', $post_int_id);
		H::css(array('home.css'));
		static::_render('post.php');
	}
}