<?php 
class home extends controller { 
	public static function _config()
	{
		static::$data->title = 'Home';
		static::$summary = ' Nós utilizamos as ferramentas e tecnologias mais modernas do mercado para criar as melhores soluções possíveis para o seu negócio.';
		static::$data->headBg = H::root() . 'files/img/layout/cloud_rise.jpg';
	}
	
	public static function index()
	{
		H::css(array('home.css'));
		static::_render('home.php');
	}
}