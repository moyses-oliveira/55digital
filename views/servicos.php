<header class="site-header common-static-header">
	<div data-stellar-background-ratio="0.25" style="background-image: url('<?php echo $headBg?>'); background-position: 10% 0px;" class="parallax-bg"></div>
	<div class="container">
		<h1 class="heading-icon-2">Serviços</h1>
	</div>
</header>
<section class="no-padding">
	<div class="page-about">
	  <div class="container">
		<div class="accordion-1 style-2 col-md-8 col-xs-12">
			<div id="accordion-service" class="panel-group">
				
			<?php
				  $tpl = 
				  '
				  <div class="panel">
					<div class="panel-heading">
						<a data-toggle="collapse" href="#service-item-2-%1$s" class="panel-title %5$s" aria-expanded="true">
							<h5>%2$s</h5>
						</a>
					</div>
					<div id="service-item-2-%1$s" class="panel-collapse collapse %4$s" aria-expanded="true">
					  <div class="panel-body">%3$s</div>
					</div>
				  </div>';
				  $servicos = modelWeblog::posts('servicos', 0, 100);
				  foreach($servicos as $k=>$a):
					printf($tpl, $k, $a->postd_vrc_title, $a->postd_txt_description, !$k ? 'in' : '', !!$k ? 'collapsed' : '' );
				  endforeach;
				?>
				</div>
			</div>
			<div class="success col-md-4 col-xs-12 quote">
			  <h4>DEPOIMENTOS</h4>
				<?php  
				$tpl = '
				<div class="page-about-item">
				  <p>%s</p>
				  <h5>%s</h5>
				</div>';
				$people = modelPeople::find();
				foreach($people as $i):
					printf($tpl, $i->people_vrc_text, $i->people_vrc_name);
				endforeach;
				  
				  
				?>
			</div>
		</div>
	</div>
</section>