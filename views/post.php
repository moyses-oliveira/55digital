<?php
$tpl = '
  <article class="post-index post-index-style-1 col-sm-offset-2 col-sm-8">
	<header class="entry-header">
	  <h3 class="post-meta color-main">{date}</h3>
	  <h2 class="post-title">{title}</h2>
	</header>
	<div class="entry-content">
	{description}
	</div>
	<footer class="entry-footer">
	  <!-- div class="author font-serif color-main"><span>Posted by Jonh Black</span></div-->
	  <ul class="social-01">
		<li><a href="#"><i class="fa fa-twitter"></i></a></li>
		<li><a href="#"><i class="fa fa-facebook"></i></a></li>
		<li><a href="#"><i class="fa fa-google-plus"> </i></a></li>
		<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
	  </ul>
	</footer>
  </article>';
?>
<header class="site-header common-static-header">
	<div data-stellar-background-ratio="0.25" style="background-image: url('<?php echo $headBg?>'); background-position: 10% 0px;" class="parallax-bg"></div>
	<div class="container">
		<h1 class="heading-icon-2"><?php echo $post->postd_vrc_title;?></h1>
	</div>
</header>
<section class="no-padding">
	<div class="page-about">
		<div class="container">
		<?php
		$url = H::link('post', URL::build($post->postd_vrc_title, $post->post_int_id));
		$find = array('{url}', '{date}','{title}', '{description}');
		$date = CData::format('d.m.Y', $post->post_dtt_posted);
		$replace = array($url, $date, $post->postd_vrc_title, $post->postd_txt_description);
		echo str_replace($find, $replace, $tpl);
		?>
		</div>
	</div>
</section>