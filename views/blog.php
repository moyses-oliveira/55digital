<?php
$tpl = '
  <article class="post-index post-index-style-1 col-sm-offset-2 col-sm-8">
	<header class="entry-header">
	  <h3 class="post-meta color-main">{date}</h3>
	  <h2 class="post-title"><a href="{url}" class="blog-link">{title}</a></h2>
	</header>
	<div class="entry-content">
	<p><a href="{url}">{description}</a></p>
	</div>
	<footer class="entry-footer">
	  <ul class="social-01">
		<li><a href="#"><i class="fa fa-twitter"></i></a></li>
		<li><a href="#"><i class="fa fa-facebook"></i></a></li>
		<li><a href="#"><i class="fa fa-google-plus"> </i></a></li>
		<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
	  </ul>
	</footer>
  </article>
  <div class="clear"></div>';
?>





<header class="site-header common-static-header">
	<div data-stellar-background-ratio="0.25" style="background-image: url('<?php echo $headBg?>'); background-position: 10% 0px;" class="parallax-bg"></div>
	<div class="container">
		<h1 class="heading-icon-2">Blog</h1>
	</div>
</header>
<section class="no-padding">
	<div class="page-about">
		<div class="container">
		<?php
		foreach($blog as $k=>$post):
			$url = H::link('post', URL::build($post->postd_vrc_title, $post->post_int_id));
			$find = array('{url}', '{date}','{title}', '{description}');
			$date = CData::format('d.m.Y', $post->post_dtt_posted);
			$replace = array($url, $date, $post->postd_vrc_title, $post->postd_vrc_summary);
			echo str_replace($find, $replace, $tpl);
		endforeach;
		?>
		</div>
	</div>
</section>