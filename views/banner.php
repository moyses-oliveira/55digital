<!--site header-->
<header class="site-header home-sticky-nav-trigger">
<div class="home-fw-slider style-7 caption-slider dir-nav parents-height">
  <div class="home-slider-item">
	<div style="background-image: url(<?php echo H::root();?>files/img/layout/devices.jpg);" class="item-image">
	  <div class="overlay">
		<div class="cell-vertical-wrapper">
		  <div class="cell-middle">
			<div class="container">
			  <div class="caption-wrapper text-center">
				<h1 class="caption">BEM VINDO À</h1>
				<h1 class="caption">55 DIGITAL</h1>
				<p class="caption">Nós temos um compromisso com seu sucesso.</p>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <div class="home-slider-item">
	<div style="background-image: url(<?php echo H::root();?>files/img/layout/men-cloud.jpg);" class="item-image">
	  <div class="overlay">
		<div class="cell-vertical-wrapper">
		  <div class="cell-middle">
			<div class="container">
			  <div class="caption-wrapper text-center">
				<h1 class="caption">BEM VINDO À</h1>
				<h1 class="caption">55 DIGITAL</h1>
				<p class="caption">Nossos clientes são nosso maior patrimônio.</p>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
  <div class="home-slider-item">
	<div style="background-image: url(<?php echo H::root();?>files/img/layout/think-big-start-small.jpg);" class="item-image">
	  <div class="overlay">
		<div class="cell-vertical-wrapper">
		  <div class="cell-middle">
			<div class="container">
			  <div class="caption-wrapper text-center">
				<h1 class="caption">BEM VINDO À</h1>
				<h1 class="caption">55 DIGITAL</h1>
				<p class="caption">Pense GRANDE comece PEQUENO.</p>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
</header>
<!--end site header-->