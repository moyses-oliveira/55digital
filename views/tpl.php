<!DOCTYPE html>
<html class="no-js">
  <?php include('tpl-head.php');?>
  <body class="corporate-page-2">
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
	<?php include('tpl-menu.php');?>
	<?php include($_content_file); ?>
	<!--site footer-->
      <footer class="footer-preset-04">
        <div>
			<a href="<?php echo URL::root();?>">
				<img src="<?php echo H::root() . 'files/img/layout/55digital-logo-bottom.png';?>" alt="Logo 55 Digital" title="logo" />
			</a>
		</div>
		<br/>
        <div class="social-04">
			<a href="<?php echo H::link('sobre-nos');?>"><i class="fa fa-building-o"></i> SOBRE NÓS</a>
			<a href="<?php echo H::link('planos');?>"><i class="fa fa-cubes"></i> PLANOS</a>
			<a href="<?php echo H::link('serviços');?>"><i class="fa fa-wrench"></i> SERVIÇOS</a>
			<a href="<?php echo H::link('portfolio');?>"><i class="fa fa-desktop"></i> PORTFÓLIO</a>
			<a href="<?php echo H::link('depoimentos');?>"><i class="fa fa-users"></i> DEPOIMENTOS</a>
			<a href="<?php echo H::link('contato-localizacao');?>"><i class="fa fa-phone"></i> CONTATO</a>
		</div>
		<ul class="social-03">
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="facebook"><i class="fa fa-facebook"></i></a></li>
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="google+"><i class="fa fa-google-plus"></i></a></li>
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="linkedin"><i class="fa fa-linkedin"></i></a></li>
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="twitter"><i class="fa fa-twitter"></i></a></li>
		</ul>
		<div class="container">
			<p class="text-right">© 2015 All Rights Reserved</p>
		</div>
      </footer>
      <!--end site footer-->
    </body>
</html>