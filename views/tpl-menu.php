<div class="before-main-nav before-main-nav-2-b1 text-center">
	<h1 style="display: none;">55 Digital</h1>
	<h2 style="font-size: 3.0em; margin-bottom: 0;">
	<a href="<?php echo H::root();?>">
		<img src="<?php echo H::root() . 'files/img/layout/55digital-logo-top.png';?>" alt="Logo 55 Digital" title="logo" />
	</a>
	</h2>
</div>
<div class="main-nav-wrapper nav-wrapper-2">
  <nav>
	<div class="main-nav main-nav-2 type-1">
	<div class="nav-logo logo-2"><a href="index.html">&nbsp;<img src="<?php echo H::root() . 'files/img/layout/55digital-logo-top.png';?>" alt="Wolverine" class="logo-2"></a></div>
	  <div class="">
		<!--div class="nav-logo"><a href="index.html"><img src="assets/images/logo-9.svg" alt="Wolverine"></a></div-->
		<ul class="nav-main-menu">
		  <li style="margin-left: 40px;"><a href="<?php echo URL::root();?>"><i class="fa fa-home"></i></a></li>
		  
		  <li><a href="<?php echo H::link('sobre-nos');?>"><i class="fa fa-building-o"></i> SOBRE NÓS</a></li>
		  <li><a href="<?php echo H::link('planos');?>"><i class="fa fa-cubes"></i> PLANOS</a></li>
		  <li><a href="<?php echo H::link('servicos');?>"><i class="fa fa-wrench"></i> SERVIÇOS</a></li>
		  <li><a href="<?php echo H::link('portfolio');?>"><i class="fa fa-desktop"></i> PORTFÓLIO</a></li>
		  <li><a href="<?php echo H::link('depoimentos');?>"><i class="fa fa-users"></i> DEPOIMENTOS</a></li>
		  <li><a href="<?php echo H::link('contato-localizacao');?>"><i class="fa fa-phone"></i> CONTATO</a></li>
		</ul>
		<ul class="nav-main-menu small-screen">
		  <li><a href="<?php echo URL::root();?>"><i class="fa fa-home"></i> HOME</a></li>
		  
		  <li><a href="<?php echo H::link('sobre-nos');?>"><i class="fa fa-building-o"></i> SOBRE NÓS</a></li>
		  <li><a href="<?php echo H::link('planos');?>"><i class="fa fa-cubes"></i> PLANOS</a></li>
		  <li><a href="<?php echo H::link('servicos');?>"><i class="fa fa-wrench"></i> SERVIÇOS</a></li>
		  <li><a href="<?php echo H::link('portfolio');?>"><i class="fa fa-desktop"></i> PORTFÓLIO</a></li>
		  <li><a href="<?php echo H::link('depoimentos');?>"><i class="fa fa-users"></i> DEPOIMENTOS</a></li>
		  <li><a href="<?php echo H::link('contato-localizacao');?>"><i class="fa fa-phone"></i> CONTATO</a></li>
		</ul>
		<!--div class="nav-search-box">
		  <form>
			<div class="content">
			  <input type="search" name="search2" value="" placeholder="Search..." class="form-control">
			  <button></button>
			</div>
		  </form>
		</div>
		<div class="nav-top-search"><a href="#" class="search-trigger-show"></a></div-->
		<div class="nav-hamburger-wrapper">
		  <div class="cell-vertical-wrapper">
			<div class="cell-middle">
			  <div class="nav-hamburger" style="margin: 25px 10px 0 0;" ><span></span></div>
			</div>
		  </div>
		</div>
		<div class="nav-social">
		  <ul class="social-01">
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="facebook"><i class="fa fa-facebook"></i></a></li>
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="google+"><i class="fa fa-google-plus"></i></a></li>
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="linkedin"><i class="fa fa-linkedin"></i></a></li>
			<li><a class="share-to" href="<?php echo $_SERVER['REQUEST_SCHEME'] . ':' . URL::atual();?>" data-sns="twitter"><i class="fa fa-twitter"></i></a></li>
		  </ul>
		</div>
	  </div>
	  <!--form class="nav-search-form">
		<div class="content-wrapper"><img src="assets/images/logo-5-white.svg" alt="logo" class="logo-light"><img src="assets/images/logo-5.svg" alt="logo" class="logo-dark">
		  <div class="content">
			<input type="search" name="search" value="" placeholder="Write search..." class="form-control">
			<button>SEARCH</button>
		  </div><a href="#" class="search-trigger-hide"></a>
		</div>
	  </form-->
	</div>
  </nav>
</div>