<div class="block-service-style-01">
	<h2><i class="fa fa-wrench"></i> &nbsp;Competências</h2>
	<div class="progressbar-wrapper">
	  
	  <div class="progressbar-container style-tooltip">
		<h5 class="progressbar-title">PHP / MySQL</h5>
		<div role="progressbar" data-transitiongoal="97" class="progressbar"></div>
	  </div>
	  <div class="progressbar-container style-tooltip">
		<h5 class="progressbar-title">HTML / CSS</h5>
		<div role="progressbar" data-transitiongoal="97" class="progressbar"></div>
	  </div>
	  <div class="progressbar-container style-tooltip">
		<h5 class="progressbar-title">JQuery / Bootstrap</h5>
		<div role="progressbar" data-transitiongoal="95" class="progressbar"></div>
	  </div>
	  <div class="progressbar-container style-tooltip">
		<h5 class="progressbar-title">Design & Digital Design</h5>
		<div role="progressbar" data-transitiongoal="92" class="progressbar"></div>
	  </div>
	  <div class="progressbar-container style-tooltip">
		<h5 class="progressbar-title">Searc Engine Optimizers (SEO)</h5>
		<div role="progressbar" data-transitiongoal="85" class="progressbar"></div>
	  </div>
	  <div class="progressbar-container style-tooltip">
		<h5 class="progressbar-title">Desenvolvimento de conteúdo exclusivo</h5>
		<div role="progressbar" data-transitiongoal="70" class="progressbar"></div>
	  </div>
	  <div class="progressbar-container style-tooltip">
		<h5 class="progressbar-title">Fotografia</h5>
		<div role="progressbar" data-transitiongoal="50" class="progressbar"></div>
	  </div>
	</div>
</div>