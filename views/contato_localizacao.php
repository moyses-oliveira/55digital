<header class="site-header common-static-header">
	<div data-stellar-background-ratio="0.25" style="background-image: url('<?php echo $headBg?>'); background-position: 10% 0px;" class="parallax-bg"></div>
	<div class="container">
		<h1 class="heading-icon-2">Contato</h1>
	</div>
</header>
<section>
<div class="container">
  <div class="row">
	<div class="page-contact">
	  <div class="col-md-6 section-block">
		<!--h5>HI! WE ARE SUPPORT</h5-->
		<h2>Fale conosco</h2>
		<div class="widget contact-us">
		  <form id="contact_form" action="<?php echo H::link(URL::friend(0), 'submit');?>">
			<input type="text" name="name" placeholder="Nome" required class="name">
			<input type="email" name="email" placeholder="Email" required class="email">
			<input type="text" name="subject" placeholder="Assunto" required >
			<textarea name="message" placeholder="Mensagem" required class="msg"></textarea>
			<button type="submit" class="normal-btn normal-btn-main btn-size-4 pull-right">
				<span style="font-size: 1.4em;"><i class="fa fa-envelope"></i> ENVIAR</span>
			</button>
		  </form>
		</div>
	  </div>
	  <div class="col-md-6 section-block">
		<h2>Contato</h2>
		<div style="padding-left: 30px;">
		<H4>contato@55digital.com.br</H4>
		<H4>TEL 41 3255-4111</H4>
		<H4>TIM 41 9737-7925</H4>
		</div>
		<!--iframe src="https://www.google.com/maps/embed/v1/place?q=SiliconValley,+California,+UnitedStates&amp;key=AIzaSyAN0om9mFmy1QN6Wf54tXAowK4eT0ZUPrU" class="map-iframe"></iframe>
		<ul class="social-03">
		  <li><a href="#"><i class="fa fa-facebook">FACEBOOK</i></a></li>
		  <li><a href="#"><i class="fa fa-twitter">TWITTER</i></a></li>
		  <li><a href="#"><i class="fa fa-pinterest-p">PINTEREST</i></a></li>
		  <li><a href="#"><i class="fa fa-instagram">INSTAGRAM</i></a></li>
		</ul-->
	  </div>
	</div>
  </div>
</div>
</section>