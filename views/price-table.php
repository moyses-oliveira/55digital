<div class="price-table">
  <div class="container remove-col-padding">
	<div class="row">
		<div class="col-sm-12">
			<div class=" price-table-content" style="padding-bottom: 0px;">
				<h5 class="h2" style="background-color: #3D465A;"><i class="fa fa-cube"></i> WEBSITE</h5>
			</div>
		</div>
	</div>
	<div class="row">
	  <div class="col-sm-4 col-xs-12">
		<div class="price-table-content">
		  <h5>STARTER</h5>
		  <h5>Layout</h5>
		  <h5>Página Home</h5>
		  <h5>Página Fale Conosco</h5>
		  <h5>Página de Portfólio</h5>
		  <h5>Painel Gerenciador de Conteúdo</h5>
		  <h5>Compartilhamento em Redes Sociais</h5>
		  <h5><span>5</span> Contas de e-mail</h5>
		  <h5><span>3GB</span> Espaço em disco</h5>
		  <h5><s><span>0</span> Páginas Adicionais</s></h5>
		  <h5><s>Blog</s></h5>
		  <h5><s>Conteúdo do Site</s></h5>
		  <h5><s>Google Analitcs</s></h5>
		  <h5><s>Consultoria SEO</s></h5>
		  <h3>R$ 120,00/mês</h3>
		  <a href="#" class="normal-btn normal-btn-main btn-size-2 subscribe">ASSINAR</a>
		</div>
	  </div>
	  <div class="col-sm-4 col-xs-12">
		<div class="price-table-content">
		  <h5>BASIC</h5>
		  <h5>Layout</h5>
		  <h5>Página Home</h5>
		  <h5>Página Fale Conosco</h5>
		  <h5>Página de Portfólio</h5>
		  <h5>Painel Gerenciador de Conteúdo</h5>
		  <h5>Compartilhamento em Redes Sociais</h5>
		  <h5><span>5</span> Contas de e-mail</h5>
		  <h5><span>5GB</span> Espaço em disco</h5>
		  <h5><span>5</span> Páginas Adicionais</h5>
		  <h5><s>Blog</s></h5>
		  <h5><s>Conteúdo do Site</s></h5>
		  <h5><s>Google Analitcs</s></h5>
		  <h5><s>Consultoria SEO</s></h5>
		  <h3>R$ 210,00/mês</h3>
		  <a href="#" class="normal-btn normal-btn-main btn-size-2 subscribe">ASSINAR</a>
		</div>
	  </div>
	  <div class="col-sm-4 col-xs-12">
		<div class="price-table-content">
		  <h5>PROFESSIONAL</h5>
		  <h5>Layout</h5>
		  <h5>Página Home</h5>
		  <h5>Página Fale Conosco</h5>
		  <h5>Página de Portfólio</h5>
		  <h5>Painel Gerenciador de Conteúdo</h5>
		  <h5>Compartilhamento em Redes Sociais</h5>
		  <h5><span>10</span> Contas de e-mail</h5>
		  <h5><span>10GB</span> Espaço em disco</h5>
		  <h5><span>10</span> Páginas Adicionais</h5>
		  <h5>Blog</h5>
		  <h5>Conteúdo do Site</h5>
		  <h5>Google Analitcs</h5>
		  <h5>Consultoria SEO</h5>
		  <h3>R$ 390,00/mês</h3>
		  <a href="#" class="normal-btn normal-btn-main btn-size-2 subscribe">ASSINAR</a>
		</div>
	  </div>
	</div>
	
	<br/>&nbsp;<br/>
	
	<div class="row">
		<div class="col-sm-12">
			<div class=" price-table-content" style="padding-bottom: 0px;">
				<h5 class="h2" style="background-color: #bA763D;"><i class="fa fa-cubes"></i> LOJA VIRTUAL</h5>
			</div>
		</div>
	</div>
	
	<div class="row">
	  <div class="col-sm-6 col-xs-12">
		<div class="price-table-content">
		  <h5>BASIC</h5>
		  <h5>Layout</h5>
		  <h5>Até 1000 produtos</h5>
		  <h5>Página Home</h5>
		  <h5>Página Fale Conosco</h5>
		  <h5>Painel Gerenciador de Conteúdo</h5>
		  <h5>Compartilhamento em Redes Sociais</h5>
		  <h5><span>7</span> Páginas Adicionais</h5>
		  <h5><span>10</span> Contas de e-mail</h5>
		  <h5><span>15GB</span> Espaço em disco</h5>
		  <h5>Blog</h5>
		  <h5>Conteúdo do Site</h5>
		  <h5>Google Analitcs</h5>
		  <h5><s>Consultoria SEO</s></h5>
		  <h3>R$ 500,00/mês</h3>
		  <a href="#" class="normal-btn normal-btn-main btn-size-2 subscribe">ASSINAR</a>
		</div>
	  </div>
	  <div class="col-sm-6 col-xs-12">
		<div class="price-table-content">
		  <h5>PROFESSIONAL</h5>
		  <h5>Layout</h5>
		  <h5>Até 3000 produtos</h5>
		  <h5>Página Home</h5>
		  <h5>Página Fale Conosco</h5>
		  <h5>Painel Gerenciador de Conteúdo</h5>
		  <h5>Compartilhamento em Redes Sociais</h5>
		  <h5><span>15</span> Páginas Adicionais</h5>
		  <h5><span>20</span> Contas de e-mail</h5>
		  <h5><span>30GB</span> Espaço em disco</h5>
		  <h5>Blog</h5>
		  <h5>Conteúdo do Site</h5>
		  <h5>Google Analitcs</h5>
		  <h5>Consultoria SEO</h5>
		  <h3>R$ 800,00/mês</h3>
		  <a href="#" class="normal-btn normal-btn-main btn-size-2 subscribe">ASSINAR</a>
		</div>
	  </div>
	</div>
	<br/>&nbsp;<br/>
  </div>
</div>