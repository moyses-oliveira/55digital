<header class="site-header common-static-header">
	<div data-stellar-background-ratio="0.25" style="background-image: url('<?php echo $headBg?>'); background-position: 10% 0px;" class="parallax-bg"></div>
	<div class="container">
		<h1 class="heading-icon-2">Depoimentos</h1>
	</div>
</header>
<section class="one-child">
<div class="container">
  <div class="row">
	<div class="col-xs-12 remove-col-padding-2">
	  <div class="testimonial-02 simple-slider dir-nav">

<?php	  
	$tpl = '
		<div class="testimonial-item clearfix">
			<div class="item-content">
				<p class="comment">%s</p>
				<p class="name text-right">%s</p>
			</div>
		</div>';
	$people = modelPeople::find();
	foreach($people as $i):
		printf($tpl, $i->people_vrc_text, $i->people_vrc_name);
	endforeach;
?>
		
	  </div>
	</div>
  </div>
</div>
</section>