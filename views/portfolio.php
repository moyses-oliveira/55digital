<header class="site-header common-static-header">
	<div data-stellar-background-ratio="0.15" style="background-image: url('<?php echo $headBg?>'); background-position: 50% 0%;" class="parallax-bg"></div>
	<div class="container">
		<h1 class="heading-icon-2">Portfolio</h1>
	</div>
</header>
<style>
	.portfolio-group .overlay-content p { font-size: 1.3em; }
</style>
<div class="homepage-portfolio-special-group section-block-p">
  <div data-filter-nav="#portfolio-external-filter" class="isotope-container">
	<div class="portfolio-group container">
	  <div class="row iso-grid">
		<?php
		$portfolio = modelWeblog::posts('portfolio', 0, 100);
		
		$thumb_path = 'files/gallery/thumbs/';
		$img_path = 'files/gallery/images/';
		$tpl = '
		<div class="grid-item portfolio-column col-lg-4 col-md-4 col-sm-6 col-xs-12 art photography">
			<a href="%1$s" class="portfolio-item portfolio-text" target="_blank">
				<img src="%2$s" alt="%3$s" title="%3$s">
				<div class="overlay dark-overlay">
				  <div class="cell-vertical-wrapper">
					<div class="cell-bottom">
					  <div class="overlay-content">
						<h5 class="color-main">%3$s</h5>
						%4$s
					  </div>
					</div>
				  </div>
				</div>
			</a>
		</div>';
		foreach($portfolio as $k=>$p):
			$path = substr($p->img_vrc_img, 0,-1 * strlen(basename($p->img_vrc_img)));
			$thumb = H::root() . ImagePlugin::resize($img_path . $p->img_vrc_img, trim($thumb_path . $path,'/'), 600, 600, false);
			printf($tpl, $p->postd_vrc_summary, $thumb, $p->postd_vrc_title, $p->postd_txt_description);
		endforeach;
		?>
		</div>
	  </div>
	</div>
  </div>
</div>