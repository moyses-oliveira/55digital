	<?php $about_us = modelWeblog::posts('about-us', 0, 2);?>
	<header class="site-header common-static-header">
		<div data-stellar-background-ratio="0.25" style="background-image: url('<?php echo $headBg?>'); background-position: 10% 0px;" class="parallax-bg"></div>
		<div class="container">
          <h1 class="heading-icon-2">Bem Vindo</h1>
		  <?php echo $about_us[0]->postd_txt_description;?>
		</div>
	</header>
      <section class="no-padding">
        <div class="page-about">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-xs-12 pragraph">
                <div class="page-about-item">
					<?php
					  $tpl = 
					  '
					  <h2>%s</h2>
						%s
					  <h5></h5>';
					  $a = $about_us[1];
					  printf($tpl, $a->postd_vrc_title, $a->postd_txt_description);
					  ?>
                </div>
				<div class="quote">
					<div class="page-about-item">
					  <h3><i>Criar grandes produtos depende de grandes pessoas.</i></h3>
					  
					  <h5><a href="https://www.google.com/intl/pt-BR/about/company/facts/">- GOOGLE -</a></h5>
					</div>
				</div>
              </div>
              <div class="col-md-6 col-xs-12 pragraph">
                <?php include 'technical.php';?>
              </div>
            </div>
          </div>
        </div>
      </section>
	  
	  
	<h2 class="heading-icon-2 text-center">Nossos Planos</h2>
	<?php include('price-table.php');?>